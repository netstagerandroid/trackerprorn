
import React, {Fragment} from 'react';
import { StyleSheet, View, Platform,StatusBar } from 'react-native';
 
import DeviceInfo from 'react-native-device-info'
import PropTypes from 'prop-types';
 
var isNotch = DeviceInfo.hasNotch();

export default class  CustomStatusbar extends React.Component
{
render(){
    return(
        <View
            style={{
                backgroundColor: '#2B82D4',
                height: Platform.OS === 'ios' ?(isNotch == true? 30:20) : 0,
            }}>
            <StatusBar
                backgroundColor={'#2B82D4'}
                barStyle='dark-content'
                // dark-content, light-content and default
                hidden={false}
                //To hide statusBar
                //Background color of statusBar
                translucent={false}
                //allowing light, but not detailed shapes
                networkActivityIndicatorVisible={true}
            />
            </View>
    )
}
}