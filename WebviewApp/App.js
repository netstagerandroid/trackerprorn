

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput, Image, Button, Alert, TouchableOpacity,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { WebView } from 'react-native-webview';
import CustomStatusbar from './src/CustomStatusbar'

const App: () => React$Node = () => {
  // let Splash_Screen = (  
  //      <View style={styles.SplashScreen_RootView}>  
  //          <View style={styles.SplashScreen_ChildView}>  
  //                <Image source={{uri:'https://static.javatpoint.com/tutorial/react-native/images/react-native-tutorial.png'}}  
  //             style={{width:'100%', height: '100%', resizeMode: 'contain'}} />  
  //         </View>  
  //      </View> )  
  const usrag = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
  return (
    <>
    <CustomStatusbar/>
    <View style={{ flex: 1}}>
      {/* <WebView source={{html:"<html><body style='color:red'>Hello<br/>This is a test</body></html>"}} style={{width:200,height:200,backgroundColor:'blue',marginTop:20}} /> */}

{/* 
      <Image
        style={{ margin: 6, width: "100%", height: 50, resizeMode: 'contain' }}
        source={require('./src/assets/vizru.png')}
      /> */}

      <WebView style={{ flex: 1, }}
        source={{ uri: 'https://gps.trackerspro.com/mobile/index.php' }}
        // source={htmlfile}
        // html={PHTML}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        scalesPageToFit={true}
        userAgent={usrag}
      />
    </View>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
